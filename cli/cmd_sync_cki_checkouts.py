"""Sync CKI checkouts to MongoDB"""

import sys
import json
import collections
from cli import misc
import pymongo


def fetch_checkouts_by_doubly_linked_list(p_head):
    l_checkout = []
    p_next = p_head
    while p_next is not None:
        print(">>> Fetch page %s ..." % p_next, file=sys.stderr)
        obj = misc.get_json_data_byurl(p_next)
        if obj is None:
            break

        p_next = obj['next']
        l_checkout.extend(obj['results'])
    return l_checkout


def get_all_checkouts(head_url):
    """https://datawarehouse.cki-project.org/api/1/kcidb/checkouts?limit=1000
    """
    return fetch_checkouts_by_doubly_linked_list(head_url)


def dump_checkouts(checkouts, dwh_checkout_prefix):
    for checkout in checkouts:
        checkout_id = misc.kv_get_raw(checkout, 'misc.iid')
        if checkout_id is None:
            continue

        checkout_url = '%s/%d' % (dwh_checkout_prefix, checkout_id)
        tree_name = misc.kv_get_raw(checkout, 'tree_name')
        redhat_id = misc.kv_get_raw(checkout, 'id')
        print('- url       : %s' % checkout_url)
        print('  tree_name : %s' % tree_name)
        print('  redhat_id : %s' % redhat_id)
        misc.print_boundary(fh=sys.stdout)


def refine_checkouts(checkouts):
    """Store misc.iid as v_iid so as to search checkouts by IID easily
    """
    l_out = []
    for checkout in checkouts:
        checkout['v_iid'] = misc.kv_get_raw(checkout, 'misc.iid')
        l_out.append(checkout)
    return l_out


def sync2mongodb(checkouts, mongodb_config):
    server = mongodb_config['server']
    port = mongodb_config['port']
    user = mongodb_config['user']
    password = mongodb_config['password']
    db = mongodb_config['db']
    table = mongodb_config['table_cki_dwh_checkout']

    # connect
    conn_str = 'mongodb://%s:%s@%s:%s/%s' % (user, password, server, port, db)
    clnt = pymongo.MongoClient(conn_str)

    # init the CKI checkouts cache
    rc = init_cki_checkouts(clnt, db, table, checkouts)

    # disconnect
    clnt.close()
    return rc


def init_cki_checkouts(clnt, db, tbl, checkouts):
    # open the table
    mydb = clnt[db]
    mytbl = mydb[tbl]

    # clean up the table
    mytbl.delete_many({})

    # insert records to the table
    mytbl.insert_many(refine_checkouts(checkouts))

    # verify
    if not misc.mongodb_verify_cki_records(mytbl, checkouts):
        print("-FAIL", file=sys.stderr)
        return False
    print("+OK", file=sys.stderr)
    return True


def main(argc, argv):
    argv0 = argv[0]
    gargs = argv[1]
    argc -= 2
    argv = argv[2:]

    if argc != 1:
        print("Usage: %s [gvars] %s" % (argv0, argv[0]),
              file=sys.stderr)
        print("e.g.", file=sys.stderr)
        print("       %s %s" % (argv0, argv[0]), file=sys.stderr)
        return 1

    verbose = misc.kv_get_raw(gargs, 'verbose')

    mongodb_config = misc.kv_get_raw(gargs, 'config.mongodb')
    if mongodb_config is None:
        return 1
    if verbose:
        print(misc.obj2json(mongodb_config), file=sys.stderr)
        misc.print_boundary(fh=sys.stderr)

    cki_config = misc.kv_get_raw(gargs, 'config.cki')
    if cki_config is None:
        return 1
    if verbose:
        print(misc.obj2json(cki_config), file=sys.stderr)
        misc.print_boundary(fh=sys.stderr)

    cki_api_checkout_prefix = misc.cki_get_api_checkout_prefix(cki_config)
    cki_checkouts = get_all_checkouts(cki_api_checkout_prefix)

    if verbose:
        cki_dwh_checkout_prefix = misc.cki_get_checkout_prefix(cki_config)
        dump_checkouts(cki_checkouts, cki_dwh_checkout_prefix)

    if not sync2mongodb(cki_checkouts, mongodb_config):
        return 1
    return 0
