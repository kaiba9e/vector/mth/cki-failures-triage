"""Sync CKI issues to MongoDB"""

import sys
import json
import collections
from cli import misc
import pymongo


def fetch_issues_by_doubly_linked_list(p_head):
    l_issue = []
    p_next = p_head
    while p_next is not None:
        print(">>> Fetch page %s ..." % p_next, file=sys.stderr)
        obj = misc.get_json_data_byurl(p_next)
        if obj is None:
            break

        p_next = obj['next']
        l_issue.extend(obj['results'])
    return l_issue


def get_all_issues(head_url):
    """https://datawarehouse.cki-project.org/api/1/issue
    """
    return fetch_issues_by_doubly_linked_list(head_url)


def get_resolved_issues(head_url):
    """https://datawarehouse.cki-project.org/api/1/issue?resolved=True
    """
    head_url_r = '%s?resolved=True' % head_url
    return fetch_issues_by_doubly_linked_list(head_url_r)


def get_unresolved_issues(head_url):
    """https://datawarehouse.cki-project.org/api/1/issue?resolved=False
    """
    head_url_u = '%s?resolved=False' % head_url
    return fetch_issues_by_doubly_linked_list(head_url_u)


def dump_issues(issues, dwh_issue_prefix):
    for issue in issues:
        issue_id = misc.kv_get_raw(issue, 'id')
        if issue_id is None:
            continue

        issue_url = '%s/%d' % (dwh_issue_prefix, issue_id)
        issue_desc = misc.kv_get_raw(issue, 'description')
        ticket_url = misc.kv_get_raw(issue, 'ticket_url')
        kind_tag = misc.kv_get_raw(issue, 'kind.tag')
        resolved = misc.kv_get_raw(issue, 'resolved')
        print('- url      : %s' % issue_url)
        print('  desc     : %s' % issue_desc)
        print('  ticket   : %s' % ticket_url)
        print('  tag      : %s' % kind_tag)
        print('  resolved : %s' % resolved)
        misc.print_boundary(fh=sys.stdout)


def sync2mongodb(issues, mongodb_config):
    server = mongodb_config['server']
    port = mongodb_config['port']
    user = mongodb_config['user']
    password = mongodb_config['password']
    db = mongodb_config['db']
    table = mongodb_config['table_cki_dwh_issue']

    # connect
    conn_str = 'mongodb://%s:%s@%s:%s/%s' % (user, password, server, port, db)
    clnt = pymongo.MongoClient(conn_str)

    # init the CKI issues cache
    rc = init_cki_issues(clnt, db, table, issues)

    # disconnect
    clnt.close()
    return rc


def init_cki_issues(clnt, db, tbl, issues):
    # open the table
    mydb = clnt[db]
    mytbl = mydb[tbl]

    # clean up the table
    mytbl.delete_many({})

    # insert records to the table
    mytbl.insert_many(issues)

    # verify
    if not misc.mongodb_verify_cki_records(mytbl, issues):
        print("-FAIL", file=sys.stderr)
        return False
    print("+OK", file=sys.stderr)
    return True


def main(argc, argv):
    argv0 = argv[0]
    gargs = argv[1]
    argc -= 2
    argv = argv[2:]

    if argc != 1:
        print("Usage: %s [gvars] %s" % (argv0, argv[0]),
              file=sys.stderr)
        print("e.g.", file=sys.stderr)
        print("       %s %s" % (argv0, argv[0]), file=sys.stderr)
        return 1

    verbose = misc.kv_get_raw(gargs, 'verbose')

    mongodb_config = misc.kv_get_raw(gargs, 'config.mongodb')
    if mongodb_config is None:
        return 1
    if verbose:
        print(misc.obj2json(mongodb_config), file=sys.stderr)
        misc.print_boundary(fh=sys.stderr)

    cki_config = misc.kv_get_raw(gargs, 'config.cki')
    if cki_config is None:
        return 1
    if verbose:
        print(misc.obj2json(cki_config), file=sys.stderr)
        misc.print_boundary(fh=sys.stderr)

    cki_api_issue_prefix = misc.cki_get_api_issue_prefix(cki_config)
    cki_issues = get_all_issues(cki_api_issue_prefix)

    if verbose:
        cki_dwh_issue_prefix = misc.cki_get_issue_prefix(cki_config)
        dump_issues(cki_issues, cki_dwh_issue_prefix)

    if not sync2mongodb(cki_issues, mongodb_config):
        return 1
    return 0
