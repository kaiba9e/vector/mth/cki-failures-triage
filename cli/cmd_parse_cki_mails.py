"""Parse CKI mail to get the checkout and save to MongoDB"""

import sys
import json
import collections
from cli import misc
import pymongo
import re


def sync2mongodb(mails, mongodb_config):
    server = mongodb_config['server']
    port = mongodb_config['port']
    user = mongodb_config['user']
    password = mongodb_config['password']
    db = mongodb_config['db']
    table = mongodb_config['table_cki_dwh_mail']

    # connect
    conn_str = 'mongodb://%s:%s@%s:%s/%s' % (user, password, server, port, db)
    clnt = pymongo.MongoClient(conn_str)

    # insert CKI mails
    rc = insert_cki_mails(clnt, db, table, mails)

    # disconnect
    clnt.close()
    return rc


def insert_cki_mails(clnt, db, tbl, mails):
    # open the table
    mydb = clnt[db]
    mytbl = mydb[tbl]

    # search record by ID, skip it if the record does exist
    new_mails = list()
    for mail in mails:
        query = {"id": mail['id']}
        ret = misc.mongodb_query_cki_records(mytbl, query)
        if ret is not None:
            continue
        new_mails.append(mail)

    # return directly if no new records
    mails = new_mails
    if not mails:
        print("+OK", file=sys.stderr)
        return True

    # insert records to the table
    mytbl.insert_many(mails)

    # verify
    if not misc.mongodb_verify_cki_records(mytbl, mails):
        print("-FAIL", file=sys.stderr)
        return False
    print("+OK", file=sys.stderr)
    return True


def get_checkout_by_mail_html(mail_html):
    """
    Check out this report and any autotriaged failures in our web dashboard:
    ...
    """
    checkout_pattern = r'>http.*/kcidb/checkouts/\d+'

    html_lines = mail_html.split('\n')
    for html_line in html_lines:
        if html_line.lower().find('href=') == -1:
            continue

        rc = re.search(checkout_pattern, html_line)
        if rc is not None:
            a, b = rc.span()
            checkout_url = html_line[a + 1:b]
            return checkout_url
    return None


def get_mail_stub_id(mail_url):
    list_url = mail_url.split('/')
    return '/'.join(list_url[-2:])


def get_checkout_id(checkout_url):
    list_url = checkout_url.split('/')
    return int(list_url[-1])


def parse_cki_mails(mail_urls):
    out = []
    for mail_url in mail_urls:
        obj_mail = {}
        obj_mail['id'] = get_mail_stub_id(mail_url)
        obj_mail['mail'] = mail_url

        mail_html = misc.get_html_byurl(mail_url)
        if mail_html is None:
            obj_mail['checkout'] = None
            obj_mail['reason'] = '-FAIL: failed to download the html of mail'
            out.append(obj_mail)
            continue

        checkout_url = get_checkout_by_mail_html(mail_html)
        if checkout_url is None:
            obj_mail['checkout'] = None
            obj_mail['reason'] = '-FAIL: checkout not found'
            out.append(obj_mail)
            continue

        obj_mail['checkout_id'] = get_checkout_id(checkout_url)
        obj_mail['checkout'] = checkout_url
        obj_mail['reason'] = '+OK'
        out.append(obj_mail)
    return out


def main(argc, argv):
    argv0 = argv[0]
    gargs = argv[1]
    argc -= 2
    argv = argv[2:]

    if argc < 2:
        print("Usage: %s [gvars] %s <mails> ..." % (argv0, argv[0]),
              file=sys.stderr)
        print("e.g.", file=sys.stderr)
        print("       %s %s" % (argv0, argv[0]), file=sys.stderr)
        return 1

    verbose = misc.kv_get_raw(gargs, 'verbose')

    mongodb_config = misc.kv_get_raw(gargs, 'config.mongodb')
    if mongodb_config is None:
        return 1
    if verbose:
        print(misc.obj2json(mongodb_config), file=sys.stderr)
        misc.print_boundary(fh=sys.stderr)

    cki_config = misc.kv_get_raw(gargs, 'config.cki')
    if cki_config is None:
        return 1
    if verbose:
        print(misc.obj2json(cki_config), file=sys.stderr)
        misc.print_boundary(fh=sys.stderr)

    mail_urls = misc.cki_uniq_urls(argv[1:])
    cki_mails = parse_cki_mails(mail_urls)
    print(misc.obj2yaml(cki_mails))

    if not sync2mongodb(cki_mails, mongodb_config):
        return 1
    return 0
