#!/usr/bin/python3
""" misc - library functions """

import sys
import json
import collections
import xmltodict
import requests
import subprocess
import re
import time
import datetime


#
# Note we use oyaml which is a drop-in replacement for PyYAML which preserves
# dict ordering. And you have to install PyYAML first, then have a try, e.g.
# ```bash
#    $ git clone https://github.com/wimglenn/oyaml.git /tmp/oyaml
#    $ export PYTHONPATH=/tmp/oyaml:$PYTHONPATH
# ```
#
try:
    import oyaml as yaml
except ModuleNotFoundError:
    import yaml


def obj2json(obj, indent=2):
    out = json.dumps(obj, indent=indent)
    return out


def obj2yaml(obj, indent=2):
    # XXX: yaml support to load both JSON and YAML
    out = yaml.dump(obj, default_flow_style=False, indent=indent)
    return out.rstrip('\n')


def load_obj_byxmlfile(xml_file):
    txt = None
    with open(xml_file, 'r') as file_handle:
        txt = ''.join(file_handle.readlines())
    obj = xmltodict.parse(txt)
    return obj


def load_obj_byyamlfile(yaml_file):
    txt = None
    with open(yaml_file, 'r') as file_handle:
        txt = ''.join(file_handle.readlines())
    obj = yaml.safe_load(txt)
    return obj


def load_obj_byjsonfile(json_file):
    txt = None
    with open(json_file, 'r') as file_handle:
        txt = ''.join(file_handle.readlines())
    obj = json.loads(txt, object_pairs_hook=collections.OrderedDict)
    return obj


def new_argv(argv0, rargv):
    argv = []
    argv.append(argv0)
    argv.extend(rargv)
    return (len(argv), argv)


def exec_shell_cmd(s_cmd):
    proc = subprocess.Popen(s_cmd, shell=True,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    (s1, s2) = proc.communicate()
    stdout = s1.decode('utf-8').rstrip('\n')
    stderr = s2.decode('utf-8').rstrip('\n')
    rc = proc.returncode
    out = (rc, stdout, stderr)
    return out


def get_html_byurl(url, retry_times=9, internal=60):
    #
    # XXX: To fetch html files from CKI dashboard on MC client (CentOS 7.9),
    #      we disable InsecureRequestWarning on purpose!
    #      Hence, we don't use         requests.get(url)
    #      but use       requests.get(url, verify=False)
    #
    from requests.packages.urllib3.exceptions import InsecureRequestWarning
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

    for i in range(retry_times):
        res = requests.get(url, verify=False)
        if res.ok:
            return res.text

        print("Oops, fail to get %s, status code: %d, reason: %s" %
              (url, res.status_code, res.reason), file=sys.stderr)
        print("#%d# sleep %d seconds and try to get %s again ......" %
              (internal, i + 1, url), file=sys.stderr)
        time.sleep(internal)
    return None


def get_html_byurl_auth(*, url, token, callee='gitlab.com'):
    # XXX: https://stackoverflow.com/questions/13825278/python-request-with-authentication-access-token
    if callee == 'gitlab.com':
    	headers = {'Authorization': 'Bearer %s' % token}
    else: # DW
    	headers = {'Authorization': 'Token %s' % token}
    res = requests.get(url, headers=headers)
    if res.ok:
        return res.text

    print("Oops, fail to get %s, %d, reason: %s" %
          (url, res.status_code, res.reason), file=sys.stderr)
    return None


def get_json_data_byurl(url, token=None, callee='gitlab.com'):
    if token is not None:
        page_html = get_html_byurl_auth(url=url, token=token, callee=callee)
    else:
        page_html = get_html_byurl(url)
    if page_html is None:
        return None
    obj = json.loads(page_html, object_pairs_hook=collections.OrderedDict)
    return obj


def _kv_safe_get(udict, key, not_empty_flag=False):
    if udict is None:
        return None

    if key not in udict:
        return None

    val = udict[key]
    if val is None:  # XXX: in case val is set as 'null'
        return None

    #
    # If not_empty_flag is set to be True, we should validate the key's
    # value. If its value is empty, must return None
    #
    if not_empty_flag:
        if not val.strip().rstrip():
            return None

    return val


def _kv_safe_get_rcrs(udict, key_str, not_empty_flag=False):
    l_key = key_str.split('.')
    for key in l_key[:-1]:
        if udict is None:
            return None
        if key not in udict:
            return None
        udict = udict[key]
    return _kv_safe_get(udict, l_key[-1], not_empty_flag)


def kv_get_raw(udict, key_str):
    return _kv_safe_get_rcrs(udict, key_str, False)


def kv_get_refined(udict, key_str):
    return _kv_safe_get_rcrs(udict, key_str, True)


def print_boundary(char='-', width=90, fh=sys.stderr):
    print(char * width, file=fh)


def get_unixtime_now():
    ts_now = datetime.datetime.utcnow().timestamp()
    return ts_now


def get_strtime_now():
    return datetime.datetime.utcnow().isoformat(timespec='milliseconds') + 'Z'


def strtime2unixtime(s, fmt='%Y-%m-%d %H:%M:%S%z'):
    dtobj = datetime.datetime.strptime(s, fmt)
    return dtobj.timestamp()


# unix time to UTC or LOCAL strtime
def unixtime2strtime(ts, tz='utc', fmt='%Y-%m-%d %H:%M:%S%z'):
    obj1 = datetime.datetime.utcfromtimestamp(ts)
    if tz == 'utc':
        obj2 = obj1.replace(tzinfo=datetime.timezone.utc)
    else:  # local time zone
        obj2 = obj1.replace(tzinfo=datetime.timezone.utc).astimezone(tz=None)
    return datetime.datetime.strftime(obj2, fmt)


def gl_strtime2unixtime(gl_strtime):
    #
    # The 'T' and 'Z' in gitlab.com strtime (e.g. in updated_at) is
    # according to ISO8601:
    #
    # Times are expressed in UTC (Coordinated Universal Time), with a
    # special UTC designator ("Z"). e.g.
    # >>> datetime.datetime.utcnow().isoformat(timespec='milliseconds') + 'Z'
    # 2022-04-13T06:56:25.816776Z
    # For more, please refer to:
    #     https://www.ietf.org/rfc/rfc3339.txt
    #
    # Since the gitlab timestamp is using UTC timezone, what we should do
    # includes:
    # a) remove 'T'
    # b) update '\.\d+Z' to be '+0000'
    #    e.g.
    #    IN  : 2021-08-23T15:15:59.396Z
    #    OUT : 2021-08-23 15:15:59+0000
    #
    # To match such a format, we should use '%Y-%m-%d %H:%M:%S%z'
    #
    ts_fmt = '%Y-%m-%d %H:%M:%S%z'
    gl_strtime = re.sub(r'T', ' ', gl_strtime)
    gl_strtime = re.sub(r'\.\d+Z', '+0000', gl_strtime)
    ts_unix = strtime2unixtime(gl_strtime, ts_fmt)
    return ts_unix


def mongodb_verify_cki_records(mongodb_table_handle, cki_records):
    exp_ids = (cki_records[-1]['id'], cki_records[0]['id'])
    record_cnt = len(exp_ids)
    for exp_id in exp_ids:
        for record in mongodb_table_handle.find({'id': exp_id}):
            if record['id'] == exp_id:
                record_cnt -= 1
                #
                # remove _id from the record because ObjectId() is
                # defined by MongoDB which is unknown to JSON
                #
                record.pop('_id')
                print(obj2json(record), file=sys.stderr)
                print_boundary(fh=sys.stderr)
    if record_cnt != 0:
        return False
    return True


def mongodb_query_cki_records(mongodb_table_handle, query=None):
    if query is None:  # == 'SELECT *'
        ret = mongodb_table_handle.find()
    else:
        ret = mongodb_table_handle.find(query)

    out = []
    for record in ret:
        record.pop('_id')
        out.append(record)
    if not out:
        return None
    return out


def cki_uniq_urls(urls):
    """ uniq and sort urls """
    urls_out = list(set(urls))
    urls_out.sort(reverse=True)
    return urls_out


def _cki_get_dwh_xurl(cki_config, key):
    dwh_home = cki_config['dwh_home']
    dwh_prefix = cki_config[key]
    xurl = '%s/%s' % (dwh_home.rstrip('/'), dwh_prefix.strip('/'))
    return xurl


def cki_get_issue_prefix(cki_config):
    return _cki_get_dwh_xurl(cki_config, 'dwh_issue_prefix')


def cki_get_api_issue_prefix(cki_config):
    return _cki_get_dwh_xurl(cki_config, 'dwh_api_issue_prefix')


def cki_get_regex_prefix(cki_config):
    return _cki_get_dwh_xurl(cki_config, 'dwh_regex_prefix')


def cki_get_api_regex_prefix(cki_config):
    return _cki_get_dwh_xurl(cki_config, 'dwh_api_regex_prefix')


def cki_get_checkout_prefix(cki_config):
    return _cki_get_dwh_xurl(cki_config, 'dwh_checkout_prefix')


def cki_get_api_checkout_prefix(cki_config):
    return _cki_get_dwh_xurl(cki_config, 'dwh_api_checkout_prefix')


def cki_get_build_prefix(cki_config):
    return _cki_get_dwh_xurl(cki_config, 'dwh_build_prefix')


def cki_get_api_build_prefix(cki_config):
    return _cki_get_dwh_xurl(cki_config, 'dwh_api_build_prefix')


def cki_get_test_prefix(cki_config):
    return _cki_get_dwh_xurl(cki_config, 'dwh_test_prefix')


def cki_get_api_test_prefix(cki_config):
    return _cki_get_dwh_xurl(cki_config, 'dwh_api_test_prefix')
