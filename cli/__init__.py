""" Main entry point and command line parsing
    o cmd_sync_cki_issues    : sync-cki-issues
    o cmd_sync_cki_regexes   : sync-cki-regexes
    o cmd_sync_cki_checkouts : sync-cki-checkouts
    o cmd_parse_cki_mails    : parse-cki-mails
    o cmd_get_cki_checkouts  : get-cki-checkouts
    o cmd_get_cki_checkout_bytest : get-cki-checkout-bytest
    o cmd_get_gitlab_issues  : get-gitlab-issues
"""

import sys
import os
import getopt
from cli import misc
from cli import cmd_sync_cki_issues
from cli import cmd_sync_cki_regexes
from cli import cmd_sync_cki_checkouts
from cli import cmd_parse_cki_mails
from cli import cmd_get_cki_checkouts
from cli import cmd_get_cki_checkout_bytest
from cli import cmd_get_gitlab_issues


def exec_command(command_name, commands):
    """Call the associated command handler"""
    try:
        command = commands[command_name]
    except KeyError:
        print('Not implemented yet', file=sys.stderr)
        return -1

    try:
        return command[0](*command[1:])
    except SystemExit:
        pass


def getenv(env, default_val):
    val = os.getenv(env)
    if val is None:
        return default_val
    else:
        return val


def _get_config(obj_db):
    d_out = {}
    for key in obj_db.keys():
        d_kv = obj_db[key]
        d_out[key] = getenv(d_kv['env'], d_kv['default'])
    return d_out


def get_config(config_file):
    obj = misc.load_obj_byyamlfile(config_file)
    config = {}
    config['mongodb'] = _get_config(obj['mongodb'])
    config['psqldb'] = _get_config(obj['psqldb'])
    config['cki'] = _get_config(obj['cki'])
    config['gitlab'] = _get_config(obj['gitlab'])
    return config


def new_argv(argv0, gvars, rargv):
    argv = []
    argv.append(argv0)
    argv.append(gvars)
    argv.extend(rargv)
    return argv


def usage(prog, cmds=None):
    print("Usage: %s [-d] <cmd> [cmd_args]" % prog, file=sys.stderr)
    if cmds is not None:
        print("  cmd: {%s}" % ', '.join(cmds), file=sys.stderr)
    return 1


def main(argc, argv):
    g_debug = False
    g_verbose = False
    g_config_file = None
    options, rargv = getopt.getopt(argv[1:], ':dvc:',
                                   ['debug', 'verbose', 'config='])
    for opt, arg in options:
        if opt in ('-d', '--debug'):
            g_debug = True
        elif opt in ('-v', '--verbose'):
            g_verbose = True
        elif opt in ('-c', '--config'):
            g_config_file = arg
        else:
            usage(argv[0])
            return 1

    g_config = None
    if g_config_file is not None:
        g_config = get_config(g_config_file)

    gvars = {
        'debug': g_debug,
        'verbose': g_verbose,
        'config': g_config
    }

    argv = new_argv(argv[0], gvars, rargv)
    argc = len(argv)
    if argc < 3:
        usage(argv[0])
        return 1

    #
    # new argv[0] is the old argv[0],
    # new argv[1] is gvars
    # new argv[2] is command name
    #
    command_name = argv[2]
    commands = {
        'sync-cki-issues': [cmd_sync_cki_issues.main, argc, argv],
        'sync-cki-regexes': [cmd_sync_cki_regexes.main, argc, argv],
        'sync-cki-checkouts': [cmd_sync_cki_checkouts.main, argc, argv],
        'parse-cki-mails': [cmd_parse_cki_mails.main, argc, argv],
        'get-cki-checkouts': [cmd_get_cki_checkouts.main, argc, argv],
        'get-cki-checkout-bytest': [cmd_get_cki_checkout_bytest.main,
                                    argc, argv],
        'get-gitlab-issues': [cmd_get_gitlab_issues.main, argc, argv],
        'help': []
    }

    commands_alias = {
        'scis': 'sync-cki-issues',
        'scre': 'sync-cki-regexes',
        'scco': 'sync-cki-checkouts',
        'pcml': 'parse-cki-mails',
        'gtco': 'get-cki-checkouts',
        'gtcobt': 'get-cki-checkout-bytest',
        'gtis': 'get-gitlab-issues',
        'help': []
    }
    if command_name in commands_alias.keys():
        command_name = commands_alias[command_name]

    if command_name == 'help':
        usage(argv[0], commands.keys())
        return 1

    return exec_command(command_name, commands)
