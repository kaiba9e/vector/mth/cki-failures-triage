""" Get checkout by a test """

import sys
import getopt
from cli import misc


def _get_url_byxid(prefix, xid):
    if '?' in prefix:
        prefix, _ = prefix.split('?')
    if isinstance(xid, int):
        xid = str(xid)
    return '%s/%s' % (prefix, xid)


def get_checkout_api_url(cki_config, checkout_id):
    prefix = misc.cki_get_api_checkout_prefix(cki_config)
    return _get_url_byxid(prefix, checkout_id)


def get_checkout_url(cki_config, checkout_id):
    prefix = misc.cki_get_checkout_prefix(cki_config)
    return _get_url_byxid(prefix, checkout_id)


def get_build_api_url(cki_config, build_id):
    prefix = misc.cki_get_api_build_prefix(cki_config)
    return _get_url_byxid(prefix, build_id)


def get_build_url(cki_config, build_id):
    prefix = misc.cki_get_build_prefix(cki_config)
    return _get_url_byxid(prefix, build_id)


def get_test_api_url(cki_config, test_xid):
    prefix = misc.cki_get_api_test_prefix(cki_config)
    return _get_url_byxid(prefix, test_xid)


def get_test_url(cki_config, test_xid):
    prefix = misc.cki_get_test_prefix(cki_config)
    return _get_url_byxid(prefix, test_xid)


def fetch(cki_config, test_xid):
    # FIXME: use dwh_token to get DW JSON data
    # dwh_token = cki_config['dwh_token']
    test_url = get_test_url(cki_config, test_xid)
    test_api_url = get_test_api_url(cki_config, test_xid)
    test_obj = misc.get_json_data_byurl(test_api_url)
    if test_obj is None:
        return None
    test_id = misc.kv_get_raw(test_obj, 'id')
    test_iid = misc.kv_get_raw(test_obj, 'misc.iid')

    build_id = misc.kv_get_raw(test_obj, 'build_id')
    if build_id is None:
        return None
    build_url = get_build_url(cki_config, build_id)
    build_api_url = get_build_api_url(cki_config, build_id)
    build_obj = misc.get_json_data_byurl(build_api_url)
    if build_obj is None:
        return None
    build_iid = misc.kv_get_raw(build_obj, 'misc.iid')

    checkout_id = misc.kv_get_raw(build_obj, 'checkout_id')
    if checkout_id is None:
        return None
    checkout_url = get_checkout_url(cki_config, checkout_id)
    checkout_api_url = get_checkout_api_url(cki_config, checkout_id)
    checkout_obj = misc.get_json_data_byurl(checkout_api_url)
    if checkout_obj is None:
        return None
    checkout_iid = misc.kv_get_raw(checkout_obj, 'misc.iid')

    out = dict()
    out['head'] = {
        'test': {
            'iid': test_iid,
            'id': test_id,
            'api': test_api_url,
            'url': test_url
        },
        'build': {
            'iid': build_iid,
            'id': build_id,
            'api': build_api_url,
            'url': build_url
        },
        'checkout': {
            'iid': checkout_iid,
            'id': checkout_id,
            'api': checkout_api_url,
            'url': checkout_url
        }
    }
    out['body'] = {
        'test': test_obj,
        'build': build_obj,
        'checkout': checkout_obj
    }
    return out


def dump_head_pretty(head):
    test = head.get('test')
    build = head.get('build')
    checkout = head.get('checkout')
    print("* Test     : %s" % test.get('url'))
    print("  * api    : %s" % test.get('api'))
    print("* Build    : %s" % build.get('url'))
    print("  * api    : %s" % build.get('api'))
    print("* Checkout : %s" % checkout.get('url'))
    print("  * api    : %s" % checkout.get('api'))


def usage(argv00, argv10):
    print("Usage: %s [gvars] %s [-H] [--pretty] <test IID or ID>" % (argv00, argv10),
          file=sys.stderr)
    print("e.g.", file=sys.stderr)
    print("       %s    %s 33679" % (argv00, argv10), file=sys.stderr)
    print("       %s    %s redhat:143464880" % (argv00, argv10),
          file=sys.stderr)


def main(argc, argv):
    argv0 = argv[0]
    gargs = argv[1]
    argc -= 2
    argv = argv[2:]

    # parse local argv
    g_dump_head_flag = False
    g_dump_head_pretty_flag = False
    options, rargv = getopt.getopt(argv[1:], ':HP', ['head', 'pretty'])
    for opt, arg in options:
        if opt in ('-H', '--head'):
            g_dump_head_flag = True
        elif opt in ('-P', '--pretty'):
            g_dump_head_pretty_flag = True
        else:
            usage(argv0, argv[0])
            return 1
    argc, argv = misc.new_argv(argv[0], rargv)
    if argc != 2:
        usage(argv0, argv[0])
        return 1

    cki_test_xid = argv[1]

    verbose = misc.kv_get_raw(gargs, 'verbose')

    cki_config = misc.kv_get_raw(gargs, 'config.cki')
    if cki_config is None:
        return 1
    if verbose:
        print(misc.obj2json(cki_config), file=sys.stderr)
        misc.print_boundary(fh=sys.stderr)

    cbt_info = fetch(cki_config, cki_test_xid)
    if verbose:
        print(misc.obj2json(cbt_info))
    else:
        if g_dump_head_flag:
            head = misc.kv_get_raw(cbt_info, 'head')
            if g_dump_head_pretty_flag:
                dump_head_pretty(head)
            else:
                print(misc.obj2json(head))
        else:
            checkout_iid = misc.kv_get_raw(cbt_info, 'head.checkout.iid')
            print(checkout_iid)

    return 0
