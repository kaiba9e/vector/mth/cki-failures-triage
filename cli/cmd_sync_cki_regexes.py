"""Sync CKI regexes to MongoDB"""

import sys
import json
import collections
from cli import misc
import pymongo


def fetch_regexes_by_doubly_linked_list(p_head):
    l_regex = []
    p_next = p_head
    while p_next is not None:
        print(">>> Fetch page %s ..." % p_next, file=sys.stderr)
        obj = misc.get_json_data_byurl(p_next)
        if obj is None:
            break

        p_next = obj['next']
        l_regex.extend(obj['results'])
    return l_regex


def get_all_regexes(head_url):
    """https://datawarehouse.cki-project.org/api/1/regex/-/regex
    """
    return fetch_regexes_by_doubly_linked_list(head_url)


def dump_regexes(regexes, dwh_regex_prefix):
    for regex in regexes:
        regex_id = misc.kv_get_raw(regex, 'id')
        if regex_id is None:
            continue

        regex_url = '%s/%d' % (dwh_regex_prefix, regex_id)
        text_match = misc.kv_get_raw(regex, 'text_match')
        file_name_match = misc.kv_get_raw(regex, 'file_name_match')
        test_name_match = misc.kv_get_raw(regex, 'test_name_match')
        issue_id = misc.kv_get_raw(regex, 'issue.id')
        print('- url             : %s' % regex_url)
        print('  issue_id        : %d' % issue_id)
        print('  text_match      : %s' % text_match)
        print('  file_name_match : %s' % file_name_match)
        print('  test_name_match : %s' % test_name_match)
        misc.print_boundary(fh=sys.stdout)


def refine_regexes(regexes):
    """Store issue.id as v_issue_id so as to search regexes by issue ID easily
    """
    l_out = []
    for regex in regexes:
        regex['v_issue_id'] = misc.kv_get_raw(regex, 'issue.id')
        l_out.append(regex)
    return l_out


def sync2mongodb(regexes, mongodb_config):
    server = mongodb_config['server']
    port = mongodb_config['port']
    user = mongodb_config['user']
    password = mongodb_config['password']
    db = mongodb_config['db']
    table = mongodb_config['table_cki_dwh_regex']

    # connect
    conn_str = 'mongodb://%s:%s@%s:%s/%s' % (user, password, server, port, db)
    clnt = pymongo.MongoClient(conn_str)

    # init the CKI regexes cache
    rc = init_cki_regexes(clnt, db, table, regexes)

    # disconnect
    clnt.close()
    return rc


def init_cki_regexes(clnt, db, tbl, regexes):
    # open the table
    mydb = clnt[db]
    mytbl = mydb[tbl]

    # clean up the table
    mytbl.delete_many({})

    # insert records to the table
    mytbl.insert_many(refine_regexes(regexes))

    # verify
    if not misc.mongodb_verify_cki_records(mytbl, regexes):
        print("-FAIL", file=sys.stderr)
        return False
    print("+OK", file=sys.stderr)
    return True


def main(argc, argv):
    argv0 = argv[0]
    gargs = argv[1]
    argc -= 2
    argv = argv[2:]

    if argc != 1:
        print("Usage: %s [gvars] %s" % (argv0, argv[0]),
              file=sys.stderr)
        print("e.g.", file=sys.stderr)
        print("       %s %s" % (argv0, argv[0]), file=sys.stderr)
        return 1

    verbose = misc.kv_get_raw(gargs, 'verbose')

    mongodb_config = misc.kv_get_raw(gargs, 'config.mongodb')
    if mongodb_config is None:
        return 1
    if verbose:
        print(misc.obj2json(mongodb_config), file=sys.stderr)
        misc.print_boundary(fh=sys.stderr)

    cki_config = misc.kv_get_raw(gargs, 'config.cki')
    if cki_config is None:
        return 1
    if verbose:
        print(misc.obj2json(cki_config), file=sys.stderr)
        misc.print_boundary(fh=sys.stderr)

    cki_api_regex_prefix = misc.cki_get_api_regex_prefix(cki_config)
    cki_regexes = get_all_regexes(cki_api_regex_prefix)

    if verbose:
        cki_dwh_regex_prefix = misc.cki_get_regex_prefix(cki_config)
        dump_regexes(cki_regexes, cki_dwh_regex_prefix)

    if not sync2mongodb(cki_regexes, mongodb_config):
        return 1
    return 0
