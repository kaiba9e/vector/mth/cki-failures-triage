"""Get all info of checkouts and dump untriaged failures"""

import sys
import json
import collections
from cli import misc
import getopt
import pymongo


def query_mail_info_from_mongodb(checkout_ids, mongodb_config):
    server = mongodb_config['server']
    port = mongodb_config['port']
    user = mongodb_config['user']
    password = mongodb_config['password']
    db = mongodb_config['db']
    table = mongodb_config['table_cki_dwh_mail']

    # connect
    conn_str = 'mongodb://%s:%s@%s:%s/%s' % (user, password, server, port, db)
    clnt = pymongo.MongoClient(conn_str)

    # query
    mydb = clnt[db]
    mytbl = mydb[table]
    out = []
    for checkout_id in checkout_ids:
        query_cond = {'checkout_id': checkout_id}
        ret = misc.mongodb_query_cki_records(mytbl, query_cond)
        co_out = {}
        if ret is None:
            co_out['checkout_id'] = checkout_id
            co_out['mail_info'] = None
        else:
            co_out['checkout_id'] = checkout_id
            co_out['mail_info'] = ret[0]  # XXX: return the 1st record if found
        out.append(co_out)

    # disconnect
    clnt.close()

    return out


def get_mail_info_bycheckout_id(mail_info_list, checkout_id):
    out = {}
    for mail_info in mail_info_list:
        if mail_info['checkout_id'] == checkout_id:
            ret = mail_info['mail_info']
            if ret is not None:
                out['id'] = ret['id']
                out['url'] = ret['mail']
                return out
    return out


def fetch_checkout_all(url):
    """ GET /api/1/kcidb/checkouts/<ID>/all """
    obj = misc.get_json_data_byurl(url + '/all')
    return obj


def fetch_checkout(api_checkout_url):
    obj_checkout = fetch_checkout_all(api_checkout_url)
    return obj_checkout


def get_checkout_api_url(cki_config, checkout_id):
    prefix = misc.cki_get_api_checkout_prefix(cki_config)
    if prefix.find('?') != -1:
        prefix, _ = prefix.split('?')
    url = '%s/%d' % (prefix, checkout_id)
    return url


def get_all_info(obj_checkout_all):
    out = {}
    out['checkout'] = obj_checkout_all['checkouts'][0]
    out['builds'] = obj_checkout_all['builds']
    out['tests'] = obj_checkout_all['tests']
    return out


def get_untriaged_builds_or_tests(list_bot):
    out = []
    for bot in list_bot:
        is_missing_triage = misc.kv_get_raw(bot, 'misc.is_missing_triage')
        if is_missing_triage:
            out.append(bot)
    return out


def get_untriaged_failures(obj_checkout_all):
    out = {}
    out['checkout'] = obj_checkout_all['checkouts'][0]
    out['builds'] = get_untriaged_builds_or_tests(obj_checkout_all['builds'])
    out['tests'] = get_untriaged_builds_or_tests(obj_checkout_all['tests'])
    return out


def get_failed_builds(builds):
    out = []
    for build in builds:
        valid = misc.kv_get_raw(build, 'valid')
        if valid is None:
            continue
        if not valid:
            out.append(build)
    return out


def get_failed_tests(tests):
    out = []
    for test in tests:
        status = misc.kv_get_raw(test, 'status')
        if status is None:
            continue
        if status.upper() != "PASS":
            out.append(test)
    return out


def get_all_failures(obj_checkout_all):
    out = {}
    out['checkout'] = obj_checkout_all['checkouts'][0]
    out['builds'] = get_failed_builds(obj_checkout_all['builds'])
    out['tests'] = get_failed_tests(obj_checkout_all['tests'])
    return out


def dump(cki_config, obj2dump, mail_info_list, verbose_flag):
    obj = {}
    obj['checkout'] = {}
    obj['report'] = {}
    obj['builds'] = []
    obj['tests'] = []

    # get checkout
    checkout_id = misc.kv_get_raw(obj2dump['checkout'], 'misc.iid')
    checkout_prefix = misc.cki_get_checkout_prefix(cki_config)
    checkout_url = '%s/%d' % (checkout_prefix, checkout_id)
    checkout_api = get_checkout_api_url(cki_config, checkout_id)
    checkout_tree_name = misc.kv_get_raw(obj2dump['checkout'], 'tree_name')
    checkout_start_time = misc.kv_get_raw(obj2dump['checkout'], 'start_time')
    obj['checkout']['url'] = checkout_url
    obj['checkout']['api'] = checkout_api
    obj['checkout']['iid'] = checkout_id
    obj['checkout']['tree'] = checkout_tree_name
    obj['checkout']['date'] = checkout_start_time
    if verbose_flag:
        obj['checkout']['raw'] = obj2dump['checkout']
    else:
        obj['checkout']['raw'] = {}

    # get report
    obj['report']['mail'] = get_mail_info_bycheckout_id(mail_info_list,
                                                        checkout_id)

    # get builds
    build_prefix = misc.cki_get_build_prefix(cki_config)
    build_api_prefix = misc.cki_get_api_build_prefix(cki_config)
    builds = obj2dump['builds']
    for build in builds:
        build_id = misc.kv_get_raw(build, 'id')
        build_arch = misc.kv_get_raw(build, 'architecture')
        build_valid = misc.kv_get_raw(build, 'valid')
        build_iid = misc.kv_get_raw(build, 'misc.iid')
        build_imt = misc.kv_get_raw(build, 'misc.is_missing_triage')

        build_url = '%s/%d' % (build_prefix, build_iid)
        build_api = '%s/%d' % (build_api_prefix, build_iid)

        build_status = 'PASS'
        if not build_valid:
            build_status = 'FAIL'

        build = {}
        build['url'] = build_url
        build['api'] = build_api
        build['iid'] = build_iid
        build['desc'] = '%s | %s | %s' % (build_id, build_arch, build_status)
        build['is_missing_triage'] = build_imt
        if verbose_flag:
            print(">>> fetch %s ..." % build_api, file=sys.stderr)
            build['raw'] = misc.get_json_data_byurl(build_api)
        else:
            build['raw'] = {}
        obj['builds'].append(build)

    # get tests
    test_prefix = misc.cki_get_test_prefix(cki_config)
    test_api_prefix = misc.cki_get_api_test_prefix(cki_config)
    tests = obj2dump['tests']
    for test in tests:
        test_id = misc.kv_get_raw(test, 'id')
        test_name = misc.kv_get_raw(test, 'comment')
        test_path = misc.kv_get_raw(test, 'path')
        test_status = misc.kv_get_raw(test, 'status')
        test_iid = misc.kv_get_raw(test, 'misc.iid')
        test_imt = misc.kv_get_raw(test, 'misc.is_missing_triage')

        test_url = '%s/%d' % (test_prefix, test_iid)
        test_api = '%s/%d' % (test_api_prefix, test_iid)

        test = {}
        test['url'] = test_url
        test['api'] = test_api
        test['iid'] = test_iid
        test['path'] = test_path
        test['desc'] = '%s | %s | %s' % (test_id, test_name, test_status)
        test['is_missing_triage'] = test_imt
        test['build_api_prefix'] = build_api_prefix
        if verbose_flag:
            print(">>> fetch %s ..." % test_api, file=sys.stderr)
            test['raw'] = misc.get_json_data_byurl(test_api)
        else:
            test['raw'] = {}
        obj['tests'].append(test)

    return obj


def usage(argv00, argv10):
    print("Usage: %s [gvars] %s [<-a|-F>] [-v] <checkout>" % (argv00, argv10),
          file=sys.stderr)
    print("e.g.", file=sys.stderr)
    print("       %s    %s 33679" % (argv00, argv10), file=sys.stderr)
    print("       %s -a %s 33679" % (argv00, argv10), file=sys.stderr)
    print("       %s -F %s 33679" % (argv00, argv10), file=sys.stderr)
    print("       %s    %s 33719 33679" % (argv00, argv10), file=sys.stderr)
    print("       %s -a %s 33719 33679" % (argv00, argv10), file=sys.stderr)
    print("       %s -F %s 33719 33679 " % (argv00, argv10), file=sys.stderr)
    print("       %s -v %s 33719 33679 " % (argv00, argv10), file=sys.stderr)


def main(argc, argv):
    argv0 = argv[0]
    gargs = argv[1]
    argc -= 2
    argv = argv[2:]

    # parse local argv
    g_all_flag = False  # get all entries
    g_fail_flag = False  # get all failed entries
    g_verbose_flag = False  # get info including raw data of builds/tests
    options, rargv = getopt.getopt(argv[1:], ':aFv',
                                   ['all', 'failed', 'verbose'])
    for opt, arg in options:
        if opt in ('-a', '--all'):
            g_all_flag = True
        elif opt in ('-F', '--failed'):
            g_fail_flag = True
        elif opt in ('-v', '--verbose'):
            g_verbose_flag = True
        else:
            usage(argv0, argv[0])
            return 1
    argc, argv = misc.new_argv(argv[0], rargv)
    if argc < 2:
        usage(argv0, argv[0])
        return 1

    # get global configs
    verbose = misc.kv_get_raw(gargs, 'verbose')

    mongodb_config = misc.kv_get_raw(gargs, 'config.mongodb')
    if mongodb_config is None:
        return 1
    if verbose:
        print(misc.obj2json(mongodb_config), file=sys.stderr)
        misc.print_boundary(fh=sys.stderr)

    cki_config = misc.kv_get_raw(gargs, 'config.cki')
    if cki_config is None:
        return 1
    if verbose:
        print(misc.obj2json(cki_config), file=sys.stderr)
        misc.print_boundary(fh=sys.stderr)

    # 1. get all checkouts ID
    checkout_ids = []
    for checkout_id_str in argv[1:]:
        checkout_id = int(checkout_id_str)
        checkout_ids.append(checkout_id)

    # 2. query MongoDB to get all mail info
    mail_info = query_mail_info_from_mongodb(checkout_ids, mongodb_config)
    if verbose:
        print(misc.obj2json(mail_info))

    # 3. query CKI dashboard (i.e. dateware house) by checkout ID one by one
    obj2dump_list = []
    for checkout_id in checkout_ids:
        api_checkout_url = get_checkout_api_url(cki_config, checkout_id)
        obj_checkout_all = fetch_checkout(api_checkout_url)

        if g_all_flag:
            obj2dump = get_all_info(obj_checkout_all)
        else:
            if g_fail_flag:
                obj2dump = get_all_failures(obj_checkout_all)
            else:
                obj2dump = get_untriaged_failures(obj_checkout_all)
        obj2dump_list.append(obj2dump)

    # 4. dump
    out_list = []
    for obj2dump in obj2dump_list:
        out = dump(cki_config, obj2dump, mail_info, g_verbose_flag)
        out_list.append(out)
    print(misc.obj2yaml(out_list))

    return 0
