""" Get gitlab.com issues for better defect tracking management

TBD: 1. Get the lastest DW test matched to the DW issue
     2. Get the maintainers from 1#.
        If it takes a lot of time, we should use MongoDB to cache DW issues
"""

import sys
import json
import collections
from cli import misc
import getopt
import gitlab
import re


g_verbose = False


def VLOG(msg):
    if g_verbose:
        print(msg, file=sys.stderr)


def has_non_admin(s, default_admins):
    # XXX: to find gitlab user
    ret = re.findall(r'@[a-zA-Z0-9_-]+', s)
    admins = [f'@{admin}' for admin in default_admins]
    for entry in ret:
        if entry not in admins:
            return True
    return False


def get_maintainers_from_body(body, default_admins):
    mainainers = set()
    ret = re.findall(r'@[a-zA-Z0-9_-]+', body)
    admins = [f'@{admin}' for admin in default_admins]
    for entry in ret:
        if entry not in admins:
            mainainers.add(entry.replace('@', ''))
    return list(mainainers)


def get_issue_notes(notes_url, token):
    out = []
    if notes_url is None:
        return out

    VLOG(f'>>> fetch notes {notes_url} ...')

    notes = misc.get_json_data_byurl(notes_url, token)
    if notes is None:
        return out
    return notes


def refine_issue_notes(notes, default_admins):
    out = list()
    for note in notes:
        body = misc.kv_get_refined(note, 'body')
        if not has_non_admin(body, default_admins):
            continue
        cell = dict()
        cell['id'] = misc.kv_get_raw(note, 'id')
        cell['created_at'] = misc.kv_get_refined(note, 'created_at')
        cell['updated_at'] = misc.kv_get_refined(note, 'updated_at')
        cell['body'] = body
        cell['maintainers'] = get_maintainers_from_body(body, default_admins)
        out.append(cell)
    return out


def uniq_maintainers(vnotes):
    maintainers = []
    for vnote in vnotes:
        for maintainer in vnote['maintainers']:
            #
            # FIXME: @mail... found
            # https://lore.kernel.org/linux-scsi/...@mail.gmail.com/T/#u
            #
            # === bogus gitlab user found ===
            #
            # XXX: Validate it via gitlab.com ???
            #      OR get maintainer from the DW issue
            #
            if maintainer in ['mail', 'redhat']:
                continue
            maintainers.append(maintainer)
    #
    # FIXME: should get the 1st maintainer according to:
    #        1) max count, e.g. l = ['a', 'b', 'a', 'b', 'c', 'a']
    #           return 'a' because l.count('a') = 3 > l.count('b') = 2
    #
    #        2) the ealiest at, e.g. l = ['a', 'b', 'a', 'b', 'c']
    #           return 'a' ? 'b' ??
    #           if (a::id < b::id) return 'a' else return 'b'
    #
    #
    return list(set(maintainers))


def _find_user(issue, gitlab_user):
    username = misc.kv_get_refined(issue, 'assignee.username')
    if (username is not None) and username == gitlab_user:
            return True

    for assignee in issue['assignees']:
        username = misc.kv_get_refined(assignee, 'username')
        if (username is not None) and username == gitlab_user:
            return True

    return False


def find_admin(issue, admin):
    return _find_user(issue, admin)


def find_re(issue, re):
    return _find_user(issue, re)


def beyond_ndays(ts_upd, ts_now, ndays):
    if (ts_now - ts_upd) / (24 * 3600) > ndays:
        return True
    return False


def parse(issues, admin, gitlab_config, iid_todump_issues):
    #
    # Reference:
    #   https://python-gitlab.readthedocs.io/en/stable/gl_objects/issues.html
    #
    default_admins = [key.strip().rstrip()
                      for key in gitlab_config['admins'].split(',')]
    token = gitlab_config['token']

    out = []
    for issue in issues:
        if iid_todump_issues and (issue.iid not in iid_todump_issues):
            continue

        VLOG(f'>>> parse issue {issue.web_url} ...')

        # XXX: Note that issue is a class instance
        # data = dict([(key, getattr(issue, key)) for key in keys])
        dt_issue = dict()
        dt_issue['id'] = issue.id
        dt_issue['iid'] = issue.iid
        dt_issue['project_id'] = issue.project_id
        dt_issue['web_url'] = issue.web_url
        dt_issue['title'] = issue.title
        dt_issue['description'] = issue.description
        dt_issue['state'] = issue.state
        dt_issue['created_at'] = issue.created_at
        dt_issue['updated_at'] = issue.updated_at
        dt_issue['closed_at'] = issue.closed_at
        dt_issue['labels'] = issue.labels
        dt_issue['milestone'] = issue.milestone
        dt_issue['assignees'] = issue.assignees
        dt_issue['assignee'] = issue.assignee
        dt_issue['author'] = issue.author
        dt_issue['type'] = issue.type
        dt_issue['issue_type'] = issue.issue_type

        # NOTE: filter it with admin to speed up
        if admin is not None:
            if not find_admin(dt_issue, admin):
                VLOG(f'>>> ... skipped as the admin is not {admin} ...')
                continue

        #
        # XXX: it may take a bit long time to download notes
        #      we don't use issue.notes.list() because it is very slow
        #
        dt_issue['user_notes_count'] = issue.user_notes_count
        dt_issue['_links'] = issue._links

        # === create v_notes for later data analysis === VERY IMPORTANT ===
        v_notes = dict()
        dt_issue['v_notes'] = v_notes

        # a) save raw user notes for later defect tracking management
        user_notes_url = misc.kv_get_refined(dt_issue['_links'], 'notes')
        user_notes = get_issue_notes(user_notes_url, token)
        v_notes['user_notes_url'] = user_notes_url
        v_notes['user_notes'] = user_notes

        # b) add more for identifying potential maintainers
        v_notes_with_at = refine_issue_notes(user_notes, default_admins)
        v_notes['v_memo'] = 'Just for identifying potential maintainers'
        v_notes['v_notes_with_at'] = v_notes_with_at
        v_notes['v_maintainers'] = uniq_maintainers(v_notes_with_at)

        #
        # XXX: issue.updated_at is not the real updated_at, to get it,
        #      we have to look into the notes
        #
        dt_issue['real_updated_at'] = get_updated_at_from_notes(user_notes)
        out.append(dt_issue)
    return out


def get_updated_at_from_notes(user_notes):
    un_sorted = sorted(user_notes, key=lambda d: d['id'])
    return un_sorted[-1]['created_at']


def filt(dt_issues, re=None, ndays=0, ntop=-1, blacklist=[]):
    out = []
    ts_now = misc.get_unixtime_now()
    for issue in dt_issues:
        VLOG('>>> filter issue %s ...' % issue['web_url'])

        issue_iid = issue.get('iid', None)
        if issue_iid in blacklist:
            VLOG(f'>>> ...skipped as the issue {issue_iid} is in blacklist {blacklist} ...')
            continue

        ts_upd = misc.gl_strtime2unixtime(issue['real_updated_at'])
        if not beyond_ndays(ts_upd, ts_now, ndays):
            continue

        if re is not None:
            if not find_re(issue, re):
                VLOG(f'>>> ...skipped as the RE is not {re} ...')
                continue

            #
            # XXX: find RE from the notes by using its 'v_maintainers'
            #
            v_maintainers = misc.kv_get_raw(issue, 'v_maintainers')
            if not v_maintainers:
                VLOG(f'>>> ...skipped as maintariners not found in notes ...')
                continue
            if re not in v_maintainers:
                VLOG(f'>>> ...skipped as the RE ({re}) not found '
                     f'in notes ({v_maintainers}) ...')
                continue

        # XXX: add a simple view for manully review
        issue_url = misc.kv_get_raw(issue, 'web_url')
        issue_title = misc.kv_get_raw(issue, 'title')
        issue_desc = misc.kv_get_raw(issue, 'description')
        dw_issue = get_dw_issue_from_description(issue_desc)
        dw_issue_regex = get_dw_issue_regex_from_description(issue_desc)
        dw_test = get_dw_test_from_description(issue_desc)
        ctd_by = misc.kv_get_raw(issue, 'author.username')
        ts_ctd = misc.gl_strtime2unixtime(issue['created_at'])
        ctd_at = misc.unixtime2strtime(ts_ctd, fmt='%Y-%m-%d %H:%M:%S')
        last_upd_at = misc.unixtime2strtime(ts_upd, fmt='%Y-%m-%d %H:%M:%S')
        last_qud_at = misc.unixtime2strtime(ts_now, fmt='%Y-%m-%d %H:%M:%S')
        issue['v_view'] = {
            'url': issue_url,
            'title': issue_title,
            'dw_issue': dw_issue,
            'dw_issue_regex': dw_issue_regex,
            'dw_test_reported': dw_test,
            'created_by': ctd_by,
            'unixtime_created_at': ts_ctd,
            'unixtime_updated_at': ts_upd,
            'unixtime_queried_at': float('%.1f' % ts_now),
            'created_at': ctd_at,
            'last_updated_at': last_upd_at,
            'last_queried_at': last_qud_at,
            'ndays_not_closed': int((ts_now - ts_ctd) // (24 * 3600)),
            'ndays_not_updated': int((ts_now - ts_upd) // (24 * 3600))
        }
        out.append(issue)

    #
    # =========================================================================
    # XXX: The unixtime stamp of the issue is smaller,
    #      the longer the issue was updated
    # =========================================================================
    #
    out_sorted = sorted(out, key=lambda d: d['v_view']['unixtime_updated_at'])
    if ntop > 0:
        return out_sorted[:ntop]
    return out_sorted


def dw_url_revise(url):
    #
    # XXX: In the history of CKI, some DW url includes '.internal'. Hence
    #      we should remove it. e.g.
    #      IN:  https://datawarehouse.internal.cki-project.org/kcidb/tests/100
    #      OUT: https://datawarehouse.cki-project.org/kcidb/tests/100
    #
    return re.sub(r'\.internal', '', url)


def get_dw_issue_from_description(desc):
    for line in desc.split('\n'):
        if not re.search(r'\.cki-project\.org/issue/\d+', line):
            continue
        results = re.findall(r'https*://\S+/issue/\d+', line)
        if results:
            return dw_url_revise(results[0])
    return None


def get_dw_issue_regex_from_description(desc):
    for line in desc.split('\n'):
        if not re.search(r'\.cki-project\.org/issue/-/regex/\d+', line):
            continue
        results = re.findall(r'https*://\S+/regex/\d+', line)
        if results:
            return dw_url_revise(results[0])
    return None


def get_dw_test_from_description(desc):
    for line in desc.split('\n'):
        if not re.search(r'\.cki-project\.org/kcidb/tests/\d+', line):
            continue
        results = re.findall(r'https*://\S+/kcidb/tests/\d+', line)
        if results:
            return dw_url_revise(results[0])
    return None


def fetch_issues(gitlab_config, state='all', project_name=None):
    server = gitlab_config['server']
    token = gitlab_config['token']
    if project_name is None:
        project_name = gitlab_config['project']

    gl = gitlab.Gitlab(server, token)
    project = gl.projects.get(project_name)
    VLOG(f'>>> project is {project.name}, its id is {project.id}')

    if state == 'all':
        issues = project.issues.list(all=True)
    else:
        issues = project.issues.list(all=True, state=state)

    return issues


def usage(argv00, argv10):
    print(f"Usage: {argv00} [gvars] {argv10} "
          f"[-s <state>] [-n ndays] [-a admin] [-r RE] <project> ",
          file=sys.stderr)
    print("e.g.", file=sys.stderr)
    print("       %s    %s cki-project/kernel-tests" % (argv00, argv10),
          file=sys.stderr)


def main(argc, argv):
    argv0 = argv[0]
    gargs = argv[1]
    argc -= 2
    argv = argv[2:]

    # parse local argv
    g_state = 'opened'  # can be in ['all', 'opened', 'closed']
    g_ndays = 0         # N days not updated, default is 0
    g_top = -1          # Top N days not updated, -1: output all issues
    g_admin = 'idorax'  # issue admin, e.g. idorax
    g_re = None         # issue RE, e.g. liwan1
    g_issues = None
    g_blacklist = []    # Black list
    options, rargv = getopt.getopt(argv[1:], ':s:n:t:a:r:i:b:',
                                   ['state=', 'ndays=', 'top=',
                                    'admin=', 're=', 'blacklist='])
    for opt, arg in options:
        if opt in ('-s', '--state'):
            g_state = arg  # ['all', 'opened', 'closed']
        elif opt in ('-n', '--ndays'):
            g_ndays = int(arg)
        elif opt in ('-t', '--top'):
            g_top = int(arg)
        elif opt in ('-a', '--admin'):
            g_admin = arg
        elif opt in ('-r', '--re'):
            g_re = arg
        elif opt in ('-i', '--issues'):
            g_issues = arg
        elif opt in ('-b', '--blacklist'):
            g_blacklist = [int(iid) for iid in arg.split(',')]
        else:
            usage(argv0, argv[0])
            return 1
    argc, argv = misc.new_argv(argv[0], rargv)
    if argc < 2:
        usage(argv0, argv[0])
        return 1

    project_name = argv[1]

    # get global configs
    global g_verbose
    g_verbose = misc.kv_get_raw(gargs, 'verbose')

    mongodb_config = misc.kv_get_raw(gargs, 'config.mongodb')
    if mongodb_config is None:
        return 1
    if g_verbose:
        print(misc.obj2json(mongodb_config), file=sys.stderr)
        misc.print_boundary(fh=sys.stderr)

    cki_config = misc.kv_get_raw(gargs, 'config.cki')
    if cki_config is None:
        return 1
    if g_verbose:
        print(misc.obj2json(cki_config), file=sys.stderr)
        misc.print_boundary(fh=sys.stderr)

    gitlab_config = misc.kv_get_raw(gargs, 'config.gitlab')
    if gitlab_config is None:
        return 1
    if g_verbose:
        print(misc.obj2json(gitlab_config), file=sys.stderr)
        misc.print_boundary(fh=sys.stderr)

    if g_issues is None:
        iid_todump_issues = []
    else:
        iid_todump_issues = [int(iid) for iid in
                             re.sub(r'[,]\s*', ' ', g_issues).split()]
        # reset related variables in case the issues are filtered
        g_state = 'all'
        g_admin = None
        g_re = None
        g_ndays = 0
        g_top = -1

    # Note each issue in [ci_issues] is a class instance
    ci_issues = fetch_issues(gitlab_config, g_state, project_name)

    # Note each issue in [dt_issues] is a dict
    dt_issues = parse(ci_issues, g_admin, gitlab_config, iid_todump_issues)

    # Note each issue in [out_issues] is a dict too
    out_issues = filt(dt_issues, g_re, g_ndays, g_top, g_blacklist)
    if g_verbose:
        print(">>> total %d issues found" % len(out_issues), file=sys.stderr)

    # OUTPUT
    print(misc.obj2json(out_issues))

    return 0
