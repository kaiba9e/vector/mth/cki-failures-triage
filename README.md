# CKI Failures Triage

A set of utilities to help to triage CKI failures.

## 1 - Sync CKI issues

```bash
  bin/ckiftadm -c etc/config.yaml    sync-cki-issues    # scis
# OR
  bin/ckiftadm -c etc/config.yaml -v sync-cki-issues
```

## 2 - Sync CKI regexes

```bash
  bin/ckiftadm -c etc/config.yaml    sync-cki-regexes   # scre
# OR
  bin/ckiftadm -c etc/config.yaml -v sync-cki-regexes
```

## 3 - Sync CKI checkouts

```bash
  bin/ckiftadm -c etc/config.yaml    sync-cki-checkouts # scco
# OR
  bin/ckiftadm -c etc/config.yaml -v sync-cki-checkouts
```

## 4 - Get checkout by parsing CKI mail

```bash
  bin/ckiftadm -c etc/config.yaml    parse-cki-mails <mail1> [mail2] ... # pcml
# OR
  bin/ckiftadm -c etc/config.yaml -v parse-cki-mails <mail1> [mail2] ...
```

## 5 - Get untriaged failures of checkouts

```bash
  bin/ckiftadm -c etc/config.yaml    get-cki-checkouts [<-a|-F>] <co> ... # gtco
# OR
  bin/ckiftadm -c etc/config.yaml -v get-cki-checkouts [<-a|-F>] <co> ...
# e.g.
  bin/ckiftadm -c etc/config.yaml -v get-cki-checkouts    33679
  bin/ckiftadm -c etc/config.yaml -v get-cki-checkouts -a 33679
  bin/ckiftadm -c etc/config.yaml -v get-cki-checkouts -F 33679

  bin/ckiftadm -c etc/config.yaml -v get-cki-checkouts    33679 33719
  bin/ckiftadm -c etc/config.yaml -v get-cki-checkouts -a 33679 33719
  bin/ckiftadm -c etc/config.yaml -v get-cki-checkouts -F 33679 33719
```


## 6 - Get checkout by test ID (or test IID)

```bash
  bin/ckiftadm -c etc/config.yaml    get-cki-checkout-bytest [-H -P] <TestID> # gtcobt
# OR
  bin/ckiftadm -c etc/config.yaml -v get-cki-checkout-bytest [-H -P] <TestID>
# e.g.
  bin/ckiftadm -c etc/config.yaml -v get-cki-checkout-bytest 3330941
  bin/ckiftadm -c etc/config.yaml    get-cki-checkout-bytest 3330941
  bin/ckiftadm -c etc/config.yaml    get-cki-checkout-bytest -H  3330941
  bin/ckiftadm -c etc/config.yaml    get-cki-checkout-bytest -HP 3330941
```


## 7 - Get opened issues of CKI project in gitlab.com

```bash

  bin/ckiftadm -c etc/config.yaml    get-gitlab-issues [-s state] [-a admin] [-r RE] [-n ndays] <project> ... # gtis
# OR
  bin/ckiftadm -c etc/config.yaml -v get-gitlab-issues [-s state] [-a admin] [-r RE] [-n ndays] <project> ...

# e.g.
  bin/ckiftadm -c etc/config.yaml -v get-gitlab-issues -s all    -a idorax redhat/centos-stream/tests/kernel/kernel-tests 
  bin/ckiftadm -c etc/config.yaml -v get-gitlab-issues -s opened -a idorax redhat/centos-stream/tests/kernel/kernel-tests 
  bin/ckiftadm -c etc/config.yaml -v get-gitlab-issues -s closed -a idorax redhat/centos-stream/tests/kernel/kernel-tests 

  bin/ckiftadm -c etc/config.yaml -v get-gitlab-issues -s opened -a idorax -n 30 redhat/centos-stream/tests/kernel/kernel-tests 
  bin/ckiftadm -c etc/config.yaml -v get-gitlab-issues -s opened -a idorax -t 5 redhat/centos-stream/tests/kernel/kernel-tests 
  bin/ckiftadm -c etc/config.yaml -v get-gitlab-issues -s opened -a idorax -n 30 -t 5 redhat/centos-stream/tests/kernel/kernel-tests 
  bin/ckiftadm -c etc/config.yaml -v get-gitlab-issues -s opened -a idorax -r liwan1 -n 30 -t 5 redhat/centos-stream/tests/kernel/kernel-tests 

  # DEFECT MANAGEMENT 01 :: send weekly reminder to RE
  #     Very senior RE: -t 3
  #          senior RE: -t 2
  #          junior RE: -t 1
  bin/ckiftadm -c etc/config.yaml -v get-gitlab-issues -s opened -a idorax -r liwan1 -n 15 -t 3 redhat/centos-stream/tests/kernel/kernel-tests

  # DEFECT MANAGEMENT 02 :: send weekly reminder to ADMIN
  #     For idorax: -n 15 -t 5
  bin/ckiftadm -c etc/config.yaml -v get-gitlab-issues -s opened -a idorax -n 15 -t 5 redhat/centos-stream/tests/kernel/kernel-tests 

  # DEFECT MANAGEMENT 03 :: get a single OR some issues via iid
  bin/ckiftadm -c etc/config.yaml -v get-gitlab-issues -iid 731     redhat/centos-stream/tests/kernel/kernel-tests 
  bin/ckiftadm -c etc/config.yaml -v get-gitlab-issues -iid 731,732 redhat/centos-stream/tests/kernel/kernel-tests 

#
# FIXME - do we need DB to record the reminder data for better defect tracking management?
#
```
